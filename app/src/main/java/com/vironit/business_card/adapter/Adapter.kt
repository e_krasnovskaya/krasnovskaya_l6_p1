package com.vironit.business_card.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.vironit.business_card.R
import com.vironit.business_card.model.Person
import com.vironit.business_card.ui.SaveActivity

class Adapter(private val persons: List<Person>) :
    RecyclerView.Adapter<Adapter.Holder>() {

    private val PERSON: String = "person"
    private val POSITION: String = "position"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item, parent, false)
        return Holder(itemView)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.name?.text = persons[position].fullName
        holder.email?.text = persons[position].email
        holder.phone?.text = persons[position].phone
        if (persons[position].bitmap != null) {
            holder.image?.setImageBitmap(persons[position].bitmap)
        }
        holder.cardView?.setOnClickListener {
            var intent = Intent(holder.itemView.context, SaveActivity::class.java)
            intent.putExtra(PERSON, persons[position])
            holder.itemView.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return persons.size
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cardView: CardView? = null
        var image: ImageView? = null
        var name: TextView? = null
        var email: TextView? = null
        var phone: TextView? = null

        init {
            cardView = itemView.findViewById(R.id.card_view)
            image = itemView.findViewById(R.id.image)
            name = itemView.findViewById(R.id.et_name)
            email = itemView.findViewById(R.id.et_email)
            phone = itemView.findViewById(R.id.et_phone)
        }
    }

}
