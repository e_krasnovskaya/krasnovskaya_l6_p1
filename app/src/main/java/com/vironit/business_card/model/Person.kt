package com.vironit.business_card.model

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Person(
    var fullName: String?,
    var email: String?,
    var phone: String?,
    var bitmap: Bitmap?
) : Parcelable

