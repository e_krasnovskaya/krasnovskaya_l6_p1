package com.vironit.business_card.ui

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.vironit.business_card.databinding.ActivityQrreaderBinding
import com.vironit.business_card.model.Person
import eu.livotov.labs.android.camview.ScannerLiveView
import eu.livotov.labs.android.camview.ScannerLiveView.ScannerViewEventListener
import eu.livotov.labs.android.camview.scanner.decoder.zxing.ZXDecoder
import org.json.JSONObject


class QRReaderActivity : AppCompatActivity() {

    private lateinit var binding: ActivityQrreaderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityQrreaderBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.scanner.scannerViewEventListener = object : ScannerViewEventListener {
            override fun onScannerStarted(scanner: ScannerLiveView) {
            }

            override fun onScannerStopped(scanner: ScannerLiveView) {
            }

            override fun onScannerError(err: Throwable) {
                Toast.makeText(
                    this@QRReaderActivity,
                    "Scanner Error: " + err.message,
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onCodeScanned(data: String) {
                AllContactsActivity.people.add(convertFromJSON(data))
                val intent = Intent(this@QRReaderActivity, AllContactsActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun convertFromJSON(card: String): Person {
        val name = card.substring(card.indexOf("me=") + 3, card.lastIndexOf(", email"))
        val email = card.substring(card.indexOf("l=") + 2, card.lastIndexOf(", phone"))
        val phone = card.substring(card.indexOf("ne=") + 3, card.lastIndexOf(", bitmap"))
//        val bitmap = card.substring(card.indexOf("p=") + 2, card.lastIndexOf(")")) as Bitmap
        return Person(name, email, phone, null)
    }


    override fun onResume() {
        super.onResume()
        val decoder = ZXDecoder()
        decoder.scanAreaPercent = 0.8
        binding.scanner.decoder = decoder
        binding.scanner.startScanner()
    }

    override fun onPause() {
        binding.scanner.stopScanner()
        super.onPause()
    }
}