package com.vironit.business_card.ui

import android.graphics.Point
import android.os.Bundle
import android.view.WindowManager
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.WriterException
import com.vironit.business_card.databinding.ActivityGenerateQractivityBinding


class GenerateQRActivity : AppCompatActivity() {

    private lateinit var qrgEncoder: QRGEncoder
    private val JSON: String = "json"
    private lateinit var binding: ActivityGenerateQractivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGenerateQractivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val manager = getSystemService(WINDOW_SERVICE) as WindowManager
        val display = manager.defaultDisplay
        val point = Point()
        display.getSize(point)
        val width: Int = point.x
        val height: Int = point.y
        var dimen = if (width < height) width else height
        dimen = dimen * 3 / 4
        qrgEncoder =
            QRGEncoder(intent.getStringExtra(JSON), null, QRGContents.Type.TEXT, dimen)
        try {
            val bitmap = qrgEncoder.encodeAsBitmap()
            binding.qr.setImageBitmap(bitmap)
        } catch (e: WriterException) {
        }
    }
}