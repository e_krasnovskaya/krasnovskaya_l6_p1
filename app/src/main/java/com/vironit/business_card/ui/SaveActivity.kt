package com.vironit.business_card.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.icu.number.IntegerWidth
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.telephony.PhoneNumberFormattingTextWatcher
import android.util.Base64
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.vironit.business_card.R
import com.vironit.business_card.databinding.ActivityMainBinding
import com.vironit.business_card.model.Person
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.util.Base64.getEncoder
import java.util.regex.Pattern


class SaveActivity : AppCompatActivity() {

    private val JSON: String = "json"
    private lateinit var binding: ActivityMainBinding
    private val CAMERA_REQUEST = 1000
    private val CAMERA_PERMISSION_CODE = 100
    private val CALL_PHONE_REQUEST = 2000
    private val CALL_PHONE_PERMISSION_CODE = 200
    private val IMAGE: String = "image"
    private val PERSON: String = "person"
    private val EDIT_IN_MENU: String = "Edit"
    private val SAVE_IN_MENU: String = "Save"
    private var isPhotographed = false
    private var bitmap: Bitmap? = null
    private val PHONE_PATTERN: Pattern =
        Pattern.compile("^(\\+375|80)(29|25|44|33)(\\d{3})(\\d{2})(\\d{2})\$")
    private val EMAIL_PATTERN: Pattern =
        Pattern.compile("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}\$")
    private var person: Person? = null
    private var personToEdit: Person? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        binding.profileImage.isEnabled = false

        if (savedInstanceState != null) {
            val bit: Bitmap? = savedInstanceState.getParcelable(IMAGE)
            if (bit != null) {
                binding.profileImage.setImageBitmap(bit)
                bitmap = bit
            }
        }

        if(intent.extras != null){
            personToEdit = intent.getParcelableExtra(PERSON)!!
            binding.etName.setText(personToEdit?.fullName)
            binding.etEmail.setText(personToEdit?.email)
            binding.etPhone.setText(personToEdit?.phone)
            if (personToEdit!!.bitmap != null) {
                binding.profileImage.setImageBitmap(personToEdit!!.bitmap)
                bitmap = personToEdit!!.bitmap
            }
        }

        binding.profileImage.setOnClickListener {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSION_CODE)
            } else {
                isPhotographed = true
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_REQUEST)
            }
        }

        binding.btnCall.setOnClickListener {
            call()
        }

        binding.btnEmail.setOnClickListener {
            sendEmail()
        }

        binding.etName.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        binding.etEmail.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        binding.etEmail.addTextChangedListener(PhoneNumberFormattingTextWatcher("BY"))

        binding.etPhone.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        if(personToEdit != null){
            binding.delete?.visibility = View.VISIBLE
        }else{
            binding.delete?.visibility = View.GONE
        }

        binding.delete?.setOnClickListener {
            var position: Int? = null
            AllContactsActivity.people.forEachIndexed { index, p ->
                if(p.fullName.equals(personToEdit!!.fullName) &&
                    p.email.equals(personToEdit!!.email) &&
                    p.phone.equals(personToEdit!!.phone)){
                    position = index
                }
            }
            AllContactsActivity.people.removeAt(position!!)
            val intent = Intent(this@SaveActivity, AllContactsActivity::class.java)
            startActivity(intent)
        }

        binding.back?.setOnClickListener {
            if (personToEdit == null && person != null && validatePhone() && validateEmail() && binding.etName.text.isNotEmpty()) {
                AllContactsActivity.people.add(person!!)
                val intent = Intent(this@SaveActivity, AllContactsActivity::class.java)
                startActivity(intent)
            } else if(personToEdit != null && person != null){
                var position: Int? = null
                AllContactsActivity.people.forEachIndexed { index, p ->
                    if(p.fullName.equals(personToEdit!!.fullName) &&
                        p.email.equals(personToEdit!!.email) &&
                            p.phone.equals(personToEdit!!.phone)){
                        position = index
                    }
                }
                AllContactsActivity.people[position!!] = person!!
                val intent = Intent(this@SaveActivity, AllContactsActivity::class.java)
                startActivity(intent)
            }else{
                val intent = Intent(this@SaveActivity, AllContactsActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun validatePhone(): Boolean {
        val isValid: Boolean
        val phoneInput = binding.etPhone.text.toString().trim()
        if (phoneInput.isEmpty()) {
            isValid = false
            Toast.makeText(
                this@SaveActivity,
                "Phone field mustn't be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (!PHONE_PATTERN.matcher(phoneInput).matches()) {
            isValid = false
            Toast.makeText(
                this@SaveActivity,
                "Enter valid phone",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            isValid = true
        }
        return isValid
    }

    private fun validateEmail(): Boolean {
        val isValid: Boolean
        val emailInput = binding.etEmail.text.toString().trim()
        if (emailInput.isEmpty()) {
            isValid = false
            Toast.makeText(
                this@SaveActivity,
                "Email field mustn't be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (!EMAIL_PATTERN.matcher(emailInput).matches()) {
            isValid = false
            Toast.makeText(
                this@SaveActivity,
                "Enter valid email",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            isValid = true
        }
        return isValid
    }

    private fun hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun encodeBitmapToBase64(image: Bitmap?): String? {
        val os = ByteArrayOutputStream()
        image?.compress(Bitmap.CompressFormat.PNG, 100, os)
        val byteArray: ByteArray = os.toByteArray()
        return getEncoder().encodeToString(byteArray)
    }

    private fun base64ToBitmap(encodedString: String?): Bitmap? {
        val decodedString: ByteArray = Base64.decode(encodedString, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(
            decodedString, 0,
            decodedString.size
        )
    }

    private fun sendEmail() {
        if (validateEmail()) {
            val emailIntent = Intent(Intent.ACTION_SENDTO)
            emailIntent.data = Uri.parse("mailto: ${binding.etEmail.text}")
            startActivity(emailIntent)
        }
    }

    private fun call() {
        if (validatePhone()) {
            if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                    arrayOf(Manifest.permission.CALL_PHONE),
                    CALL_PHONE_PERMISSION_CODE
                )
            } else {
                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse("tel:${binding.etPhone.text}")
                startActivityForResult(callIntent, CALL_PHONE_REQUEST)
            }
        }
    }

    private fun editCard(item: MenuItem) {
        item.title = SAVE_IN_MENU
        binding.etName.isFocusableInTouchMode = true
        binding.etEmail.isFocusableInTouchMode = true
        binding.etPhone.isFocusableInTouchMode = true
        binding.profileImage.isEnabled = true
        binding.btnCall.visibility = View.GONE
        binding.btnEmail.visibility = View.GONE
    }

    private fun saveCard(item: MenuItem) {
        if (validateEmail() && validatePhone() && binding.etName.text.isNotEmpty()) {
            item.title = EDIT_IN_MENU
            binding.etName.isEnabled = false
            binding.etEmail.isEnabled = false
            binding.etPhone.isEnabled = false
            binding.profileImage.isEnabled = false
            binding.btnCall.visibility = View.VISIBLE
            binding.btnEmail.visibility = View.VISIBLE
            person = Person(
                binding.etName.text.toString(),
                binding.etEmail.text.toString(),
                binding.etPhone.text.toString(),
                bitmap
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_PERMISSION_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, CAMERA_REQUEST)
        }
        if (requestCode == CALL_PHONE_PERMISSION_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            val callIntent = Intent(Intent.ACTION_DIAL)
            callIntent.data = Uri.parse("tel:${binding.etPhone.text}")
            startActivityForResult(callIntent, CALL_PHONE_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            bitmap = data?.extras?.get("data") as Bitmap
            binding.profileImage.setImageBitmap(bitmap)
            isPhotographed = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_edit -> {
                if (item.title.equals(EDIT_IN_MENU)) {
                    editCard(item)
                } else {
                    saveCard(item)
                }
            }
            R.id.menu_create_QR -> {
                val intent = Intent(this@SaveActivity, GenerateQRActivity::class.java)
                intent.putExtra(JSON, createJSON())
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createJSON(): String {
        var json = JSONObject()
        var jsonPerson = Person(
            binding.etName.text.toString(),
            binding.etEmail.text.toString(),
            binding.etPhone.text.toString(),
            bitmap
        )
        json.put(PERSON, jsonPerson)
        return json.toString()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (!isPhotographed) {
            outState.putParcelable("image", bitmap)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}